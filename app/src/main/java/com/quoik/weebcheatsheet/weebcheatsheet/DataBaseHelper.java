package com.quoik.weebcheatsheet.weebcheatsheet;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Gab on 15/09/2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "Katakana.db";
    public static final String TABLE_NAME = "Katakana";
    public static final String COL_1 = "Syllable";
    public static final String COL_2 = "Katakana";
    private final SQLiteDatabase db;

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, 3);
        this.db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Katakana (syllable TEXT PRIMARY KEY, Katakana TEXT)");
        db.execSQL("INSERT INTO Katakana (syllable, Katakana) values(\"A\", \"あ\")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + this.TABLE_NAME);
        this.onCreate(db);
    }

    public String findKatakana(String syllable){
        String Katakana = "";

        Cursor c = this.db.rawQuery("SELECT Katakana FROM Katakana WHERE syllable = ?", new String[] {syllable});

        if (c.moveToFirst()){
            Katakana = c.getString(0);
        }

        return Katakana;
    }
}
