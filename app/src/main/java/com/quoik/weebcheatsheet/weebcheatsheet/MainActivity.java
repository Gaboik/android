package com.quoik.weebcheatsheet.weebcheatsheet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private DataBaseHelper database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new DataBaseHelper(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button infoBtn = (Button) findViewById(R.id.infoBtn);
        infoBtn.setEnabled(false);

        final Button searchButton = (Button)findViewById(R.id.searchBtn);
        final Button informationBtn = (Button)findViewById(R.id.infoBtn);
        final EditText syllableInput = (EditText)findViewById(R.id.syllableInput);

        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String wantedSyllable = syllableInput.getText().toString();

                TextView textDisplay = (TextView)findViewById(R.id.displayText);

                if (wantedSyllable.isEmpty()) {
                    textDisplay.setText("Aucun Katakana à afficher");
                    if (informationBtn.isEnabled())
                        informationBtn.setEnabled(false);
                } else {
                    if (!informationBtn.isEnabled())
                        informationBtn.setEnabled(true);
                    String kanji = database.findKatakana(wantedSyllable);
                    if (kanji.length() > 0) {
                        textDisplay.setText(kanji);
                    }
                    else{
                        textDisplay.setText("Aucun kanji ne correspond à votre recherche");
                    }
                }
            }
        });

        informationBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, information.class);
                myIntent.putExtra("syllable", syllableInput.getText().toString());
                MainActivity.this.startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_credits) {
            Intent myIntent = new Intent(this, Credits.class);
            this.startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }

}
